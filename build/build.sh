#!/bin/sh
set -e

# --------------------------------
# Parameters

if [ "$DEBUG" ]; then
   set -ux
fi

# export DOCKER_BUILDKIT=0

# shellcheck disable=SC1091
. "$(pwd)"/build.args

TIMESTAMP=$(date +%Y%m%d.%H%M)
export TIMESTAMP

IMAGE_NAME=${IMAGE_NAME:-trivy}
export IMAGE_NAME

IMAGE_TAG=${IMAGE_TAG:-$TRIVY}
export IMAGE_TAG


# --------------------------------
# Functions

docker_pull() {
    docker pull "$1" | grep -e Digest -e Status -e Error;
}


# --------------------------------
# Run

# base images
for BASE in $(grep "FROM" Dockerfile | cut -d" " -f2 | sort | uniq)
do
   eval docker_pull "$BASE"
done

# generate build-args
BUILD_ARGS=$(while IFS= read -r line; do printf "%s" "--build-arg $line "; done < build.args)

# build image
if [ -z "$DEBUG" ]; then
   set -ux
fi

# shellcheck disable=SC2086
docker build --no-cache --force-rm $BUILD_ARGS -t "$IMAGE_NAME":"$IMAGE_TAG"-"$TIMESTAMP" .
