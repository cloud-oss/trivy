# Description

This repository provides an extended version of trivy (https://github.com/aquasecurity/trivy)
- embedded vulnerability database
- embedded config policies
